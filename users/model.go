package users

import (

"errors"
"github.com/musamusa/db-connect"
"github.com/pborman/uuid"
"time"

)

type User struct {
	Id        	 string `xorm:"pk '_id'" json:"_id"`
	Username 	 string `xorm:"varchar(50) notnull unique" json:"username"`
	Password 	 string `json:"password"`
	FirstName    string `json:"first_name"`
	LastName 	 string `json:"last_name"`
	Age          int `json:"age"`
	Birthday     *time.Time `json:"birthday"`
	Email        string  `xorm:"unique" json:"email"`
	Role         string  `json:"role"`
	Phone 		 string `json:"phone"`
	CreatedAt	 *time.Time `xorm:"created" json:"created_at"`
	UpdatedAt	 *time.Time `xorm:"updated" json:"updated_at"`
}

func Create (user User) (string, error) {
	orm, err := db_connect.SyncDB(&User{})

	if err != nil {
		return "", err
	}

	user.Id = uuid.New()
	_, err = orm.Insert(user)
	return user.Id, err
}

func All () ([]User, error) {
	orm, err := db_connect.SyncDB(&User{})
	users := make([]User, 0)
	if err != nil {
		return users, err
	}
	err = orm.Find(&users)
	return users, err
}

func Get (id string) (User, error){
	if len(id) == 0 {
		return User{}, errors.New("no ID Provided")
	}
	user := User{}
	orm, err := db_connect.SyncDB(&User{})
	if err != nil {
		return user, err
	}
	has, err := orm.Id(id).Get(&user)
	if has {
		return user, nil
	}
	return user, err
}

func Update (user User) (string, error){
	id := user.Id
	if len(id) == 0 {
		return Create(user)
	}
	orm, err := db_connect.SyncDB(&User{})
	if err != nil {
		return id, err
	}
	affected, err := orm.Id(id).Update(user)

	if affected > 0 {
		return id, nil
	}
	return id, err
}