package users

import "github.com/kataras/iris"

/**
* @method Routes
 */
func Routes(app *iris.Application) {
	var controller = UserController{}
	usersRoutes := app.Party("/user", usersRouteMiddleware)
	usersRoutes.Get("/", controller.All)
	usersRoutes.Get("/me", controller.GetMe)
	usersRoutes.Get("/{id:string}", controller.Get)
	usersRoutes.Post("/", controller.Create)
	usersRoutes.Put("/{id:string}", controller.Update)
}

func usersRouteMiddleware(ctx iris.Context) {
	ctx.Next() // to move to the next handler, or don't that if you have any auth logic.
}
