package users

import (
	"errors"
	"fmt"
	"github.com/kataras/iris"
	"github.com/kataras/iris/sessions"
	"gitlab.com/musamusa/users-management/utils"
)

// UserController is our /user controller.
// UserController is responsible to handle the following requests:
// GET 				/user/login
// POST 			/user/login
// GET 				/user/me
// All HTTP Methods /user/logout
type UserController struct {
	// context is auto-binded by Iris on each request,
	// remember that on each incoming request iris creates a new UserController each time,
	// so all fields are request-scoped by-default, only dependency injection is able to set
	// custom fields like the Service which is the same for all requests (static binding)
	// and the Session which depends on the current context (dynamic binding).
	// Session, binded using dependency injection from the main.go.
	Session sessions.Session
}

const userIDKey = "UserID"

func (c *UserController) getCurrentUserID() string {
	userID := c.Session.GetStringDefault(userIDKey, "")
	return userID
}

func (c *UserController) isLoggedIn() bool {
	return len(c.getCurrentUserID()) > 0
}

func (c *UserController) logout() {
	c.Session.Destroy()
}

// GetMe handles GET: http://localhost:8080/user/me.
func (c *UserController) GetMe(ctx iris.Context) {
	id := c.getCurrentUserID()
	u, err := Get(id)
	if err != nil {
		ctx.StatusCode(iris.StatusUnauthorized)
		ctx.JSON(utils.ErrorToObject(errors.New("unauthorized access")))
	} else {
		ctx.JSON(u)
	}
}

// AnyLogout handles All/Any HTTP Methods for: http://localhost:8080/user/logout.
func (c *UserController) AnyLogout (ctx iris.Context) {
	if c.isLoggedIn() {
		c.logout()
	}
}

func (c *UserController) Create (ctx iris.Context) {
	user := User{}
	err := ctx.ReadJSON(&user)
	var userId string
	if err == nil {
		userId, err = Create(user)
	}

	if err != nil {
		ctx.StatusCode(iris.StatusBadRequest)
		ctx.JSON(utils.ErrorToObject(err))
	} else {
		ctx.JSON(utils.GenericObject(userId))
	}
}

func (c *UserController) Update (ctx iris.Context) {
	id := ctx.Params().Get("id")
	user := User{}
	err := ctx.ReadJSON(&user)
	if err == nil {
		id, err = Update(user)
	}

	if err != nil {
		ctx.StatusCode(iris.StatusBadRequest)
		ctx.JSON(utils.ErrorToObject(err))
	} else {
		ctx.JSON(utils.GenericObject(id))
	}
}

func (c *UserController) Get (ctx iris.Context) {
	id := ctx.Params().Get("id")
	user, err := Get(id)

	if len(user.Id) == 0 {
		ctx.StatusCode(iris.StatusNotFound)
		ctx.JSON(utils.GenericObject("User not found."))
	} else if err != nil {
		ctx.StatusCode(iris.StatusInternalServerError)
		fmt.Println(err)
		ctx.JSON(utils.ErrorToObject(err))
	} else {
		ctx.JSON(user)
	}
}

func (c *UserController) All (ctx iris.Context) {
	users, err := All()
	if err != nil{
		ctx.StatusCode(iris.StatusInternalServerError)
		ctx.JSON(utils.ErrorToObject(err))
	} else {
		ctx.JSON(users)
	}
}
