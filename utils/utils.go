package utils


type ErrorMessage struct {
	Message string `json:"message"`
}

type GenericMessage struct {
	Message interface{} `json:"message"`
}

func ErrorToObject (err error) ErrorMessage {
	return ErrorMessage{Message: err.Error()}
}

func GenericObject (bean interface{}) GenericMessage{
	return GenericMessage{Message: bean}
}