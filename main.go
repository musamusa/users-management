package main

import (
	"github.com/kataras/iris"
	"gitlab.com/musamusa/users-management/users"
)

func main() {
	app := iris.New()

	// yaag.Init(&yaag.Config{ // <- IMPORTANT, init the middleware.
	// 	On:       true,
	// 	DocTitle: "Iris",
	// 	DocPath:  "./templates/api-doc.html",
	// 	BaseUrls: map[string]string{"Production": "", "Staging": ""},
	// })
	// app.Use(irisyaag.New())

	app.Logger().SetLevel("debug")

	// app.Use(recover.New())
	// app.Use(logger.New())


	// customLogger := logger.New(logger.Config{
	// 	// Status displays status code
	// 	Status: true,
	// 	// IP displays request's remote address
	// 	IP: true,
	// 	// Method displays the http method
	// 	Method: true,
	// 	// Path displays the request path
	// 	Path: true,
	//
	// 	// Columns: true,
	//
	// 	// if !empty then its contents derives from `ctx.Values().Get("logger_message")
	// 	// will be added to the logs.
	// 	MessageContextKeys: []string{"logger_message"},
	//
	// 	// if !empty then its contents derives from `ctx.GetHeader("User-Agent")
	// 	MessageHeaderKeys: []string{"User-Agent"},
	// })
	//
	// app.Use(customLogger)

	users.Routes(app)

	// app.RegisterView(iris.HTML("./", ".html"))
	//
	// app.Get("/api-doc", func(ctx iris.Context) {
	// 	if err := ctx.View("api-doc.html"); err != nil {
	// 		ctx.Application().Logger().Infof(err.Error())
	// 	}
	// })


	app.Run(iris.Addr(":7000"), iris.WithoutServerError(iris.ErrServerClosed))
}